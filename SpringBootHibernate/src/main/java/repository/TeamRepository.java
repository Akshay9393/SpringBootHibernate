package repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import model.Team;

@Repository
public interface TeamRepository extends CrudRepository<Team, Long> {

    Team findByPlayers(long playerId);

	Team findOne(long l);

}
